# Parse Email
Parsing mails messages from file on link and

 1. Discovering Average value param "X-DSPAM-Probability" on command line
 2. Building Bar Chart
 
#Demo

Average value X-DSPAM-Probability param in terminal:

![Average value X-DSPAM-Probability param](./images/1.png)

Bar Chart for senders:

![Bar Chart for senders](./images/2.png)


Pie Chart for senders:

![Pie Chart for senders](./images/3.png)
 
## Data
Input

 * http://www.pythonlearn.com/code3/mbox.txt (file: Mails Journal)
 
Output

 * Average value param "X-DSPAM-Probability" on command line
 * Build Bar Chart for senders
 * Additional: Build Pie Chart for senders